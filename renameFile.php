<!DOCTYPE html>
<html style="font-size: 16px;">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Переименовыватель файлов</title>
    <link rel="stylesheet" href="src/nicepage.css" media="screen">
    <link rel="stylesheet" href="src/style.css" media="screen">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Преобразователь файлов">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
</head>
<body class="u-body u-xl-mode">
<header class="u-clearfix u-header u-header" id="sec-52c4">
    <div class="u-align-left u-clearfix u-sheet u-sheet-1">
        <a href="https://nicepage.com/c/industrial-website-templates" class="u-image u-logo u-image-1" data-image-width="1280" data-image-height="1280">
            <img src="src/images/5a7d3e974231a595bef58dfa01cd91199f915a330120a685e8eefec82f665a05cf2846d2e3cbf6b731b05d544194671f1cb08e25ab02b3d6b6088b_1280.png" class="u-logo-image u-logo-image-1">
        </a>
        <h3 class="u-custom-font u-font-open-sans u-headline u-text u-text-custom-color-1 u-text-1">
            <a href="/"> File-writer</a>
        </h3>
    </div>
</header>
<section class=" u-section-123">
    <div class="forms">
        <form class="main-form u-form" enctype="multipart/form-data" action="/runRename.php" method="POST">
            <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
            <div class="u-input main-input">
                <p>Отправить этот файл:</p> <input name="userfile" type="file" />
            </div>
            <div class="u-form-submit">
                <input type="submit" value="Отправить файл" />
            </div>
        </form>
    </div>
</section>
<?php



?>
<footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-f6f9">
    <div class="u-clearfix u-sheet u-sheet-1">
        <p class="u-small-text u-text u-text-variant u-text-1">Сервис предназначен упростить работу с файлами</p>
    </div>
</footer>
</body>
</html>

