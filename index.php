<?php
/*$zip = new ZipArchive();
$filename = "file/test112.zip";

if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
    exit("Невозможно открыть <$filename>\n");
}

$zip->addFromString("testfilephp.txt" . time(), "#1 Это тестовая строка добавляется в качестве testfilephp.txt.\n");
$zip->addFromString("testfilephp2.txt" . time(), "#2 Это тестовая строка добавляется в качестве testfilephp2.txt.\n");
$zip->addFile("/index.php","/testfromfile.php");
echo "numfiles: " . $zip->numFiles . "\n";
echo "status:" . $zip->status . "\n";
$zip->close();*/

//$zip = zip_open("/file/web.zip");
//if ($zip) {
//    while ($zip_entry = zip_read($zip)) {
//        echo "Название:         " . zip_entry_name($zip_entry) . "\n";
//        echo "Исходный размер:  " . zip_entry_filesize($zip_entry) . "\n";
//        echo "Сжатый размер:    " . zip_entry_compressedsize($zip_entry) . "\n";
//        echo "Метод сжатия:     " . zip_entry_compressionmethod($zip_entry) . "\n";
//
//        if (zip_entry_open($zip, $zip_entry, "r")) {
//            echo "Содержимое файла:\n";
//            $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
//            echo "$buf\n";
//
//            zip_entry_close($zip_entry);
//        }
//        echo "\n";
//
//    }
//
//    zip_close($zip);
//
//}
?>
<!DOCTYPE html>
<html style="font-size: 16px;">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Преобразователь файлов</title>
    <link rel="stylesheet" href="src/nicepage.css" media="screen">
    <link rel="stylesheet" href="src/style.css" media="screen">
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <link id="u-page-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i">
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Преобразователь файлов">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
</head>
<body class="u-body u-xl-mode"><header class="u-clearfix u-header u-header" id="sec-52c4"><div class="u-align-left u-clearfix u-sheet u-sheet-1">
        <a href="https://nicepage.com/c/industrial-website-templates" class="u-image u-logo u-image-1" data-image-width="1280" data-image-height="1280">
            <img src="src/images/5a7d3e974231a595bef58dfa01cd91199f915a330120a685e8eefec82f665a05cf2846d2e3cbf6b731b05d544194671f1cb08e25ab02b3d6b6088b_1280.png" class="u-logo-image u-logo-image-1">
        </a>
        <h3 class="u-custom-font u-font-open-sans u-headline u-text u-text-custom-color-1 u-text-1">
            <a href="/"> File-writer</a>
        </h3>
    </div></header>
<section class="u-align-center u-clearfix u-valign-middle u-section-1" id="sec-c3e5">
    <div class="u-clearfix u-expanded-width u-gutter-0 u-layout-wrap u-layout-wrap-1">
        <div class="u-layout" style="">
            <div class="u-layout-row" style="">
                <div class="u-align-left u-container-style u-image u-layout-cell u-left-cell u-size-30 u-size-xs-60 u-image-1" src="" data-image-width="1280" data-image-height="1280">
                    <div class="u-container-layout u-valign-middle u-container-layout-1" src=""></div>
                </div>
                <div class="u-align-left u-container-style u-layout-cell u-palette-1-base u-right-cell u-size-30 u-size-xs-60 u-layout-cell-2">
                    <div class="u-container-layout u-valign-middle u-container-layout-2">
                        <h2 class="u-text u-text-default u-text-1">Rename File</h2>
                        <p class="u-text u-text-2">Если необходимо переименовать кучу файлов или изменить их названия на нужные вам, или убрать из имён ненужные символы, то жми сюда.</p>
                        <a href="/renameFile.php" class="u-border-2 u-border-white u-btn u-btn-rectangle u-button-style u-none u-btn-1">Начать работу</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<footer class="u-align-center u-clearfix u-footer u-grey-80 u-footer" id="sec-f6f9"><div class="u-clearfix u-sheet u-sheet-1">
        <p class="u-small-text u-text u-text-variant u-text-1">Сервис предназначен упростить работу с файлами</p>
    </div></footer>

</body>
</html>
